# Software Studio 2019 Spring Assignment_02

## Topic
* Project Name : Assignment_02 (Raiden)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animation|10%|Y|
|Particle Systems|10%|N|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|

## Bonus
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-player(off-line)|5%|N|
|Bullet automatic aiming|5%|N|
|Unique bullet|5%|N|
|Little helper|5%|N|
|Boss unique movement without attack-mode|?%|N|

## Website Detail Description

# 作品網址：https://106062371.gitlab.io/Assignment_02

# Jucify mechanisms : 
敵人： 固定時間產生，每個敵人各自隨機改變方向、射擊等等。
玩家: 上下左右鍵控制移動，Z鍵射擊，遊戲中按P可以暫停遊戲，玩家被敵人射到扣一條血，未能將敵機擊落超出螢幕範圍扣一條血，扣滿三條遊戲結束。
