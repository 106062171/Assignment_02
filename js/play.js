let enemies_fire;
let heart1;
let heart2;
let heart3;
let score;
let play_bgm;
let hitsound;
let explosion;
let play_state = {
	create: function(){
		play_bgm = game.add.audio("play_bgm");
		play_bgm.play();
		hitsound = [];
		hitsound.push(game.add.audio("hiteffect1"));
		hitsound.push(game.add.audio("hiteffect2"));
		hitsound.push(game.add.audio("hiteffect3"));
		hitsound.push(game.add.audio("hiteffect4"));
		hitsound.push(game.add.audio("hiteffect5"));
		hitsound.push(game.add.audio("hiteffect6"));
		this.firesound = game.add.audio("firesound");
		explosion = game.add.audio("explosion");
		this.text_style = {font: "36px Arial", fill: "White"};
		this.hover_style = {font: "36px Arial", fill: "LightBlue"};
		this.press_style = {font: "36px Arial", fill: "FireBrick"};
		this.player_speed = 10;
		this.period_hair1 = 20;
		this.period_hair2 = 15;
		this.period_hair3 = 30;
		this.period_hair4 = 15;
		this.period_hair5 = 10;
		this.period_shoot = 5;
		this.count1 = 0;
		this.count2 = 0;
		this.count3 = 0;
		this.count4 = 0;
		this.count5 = 0;
		this.count6 = 0;
		this.d6 = 0;
		this.cursor = game.input.keyboard.createCursorKeys();
		this.shoot_key = game.input.keyboard.addKey(Phaser.KeyCode.Z);
		create_background(this);
		create_player(this, 1);
		this.enemies = game.add.group();
		this.enemies.enableBody = true;
		game.time.events.loop(3000, this.create_enemy, this);
		this.gunfire.animations.add("fire", [0, 1, 2, 3], 8, false);
		this.bullets = game.add.group();
		this.bullets.enableBody = true;
		enemies_fire = game.add.group();
		enemies_fire.enableBody = true;
		let scoreboard = game.add.image(5, 5, "scoreboard");
		scoreboard.scale.setTo(1, 0.6);
		this.scoreboard = game.add.text(30, 30, "Score: 0", this.text_style)
		score = 0;
		heart1 = game.add.image(450, 30, "heart");
		heart2 = game.add.image(500, 30, "heart");
		heart3 = game.add.image(550, 30, "heart");
		this.pause_key = game.input.keyboard.addKey(Phaser.KeyCode.P);
		this.pause_key.onDown.add(function(){
			if(game.paused){
				this.mask.visible = false;
				this.pauseboard.visible = false;

				this.resume_btn.visible = false;
				this.resume_text.visible = false;

				this.restart_btn.visible = false;
				this.restart_text.visible = false;

				this.back_to_menu_btn.visible = false;
				this.back_to_menu_text.visible = false;

				game.paused = false;
			} else{
				game.paused = true;

				this.mask.visible = true;
				this.pauseboard.visible = true;

				this.resume_btn.visible = true;
				this.resume_text.visible = true;

				this.restart_btn.visible = true;
				this.restart_text.visible = true;

				this.back_to_menu_btn.visible = true;
				this.back_to_menu_text.visible = true;
			}
		}, this);
		this.mask = game.add.image(0, 0, "mask");
		this.mask.visible = false;
		this.pauseboard = game.add.image(game.width/2, game.height/2, "board");
		this.pauseboard.scale.setTo(0.8, 1);
		this.pauseboard.anchor.setTo(0.5, 0.5);
		this.pauseboard.visible = false;

		this.resume_btn = game.add.button(game.width/2, 200, "button", resume, this, 1, 0, 2, 0);
		this.resume_btn.scale.setTo(0.5, 0.5);
		this.resume_btn.anchor.setTo(0.5, 0.5);
		this.resume_btn.visible = false;
		this.resume_text = game.add.text(game.width/2, 200, "繼續遊戲", this.text_style);
		this.resume_text.anchor.setTo(0.5, 0.5);
		this.resume_text.visible = false;
		this.resume_btn.onInputOver.add( function(){
			this.resume_text.setStyle(this.hover_style);
		}, this);
		this.resume_btn.onInputDown.add(function(){
			this.resume_text.setStyle(this.press_style);
		}, this);
		this.resume_btn.onInputUp.add(function(){
			this.resume_text.setStyle(this.hover_style);
		}, this);
		this.resume_btn.onInputOut.add(function(){
			this.resume_text.setStyle(this.text_style);
		}, this);

		this.restart_btn = game.add.button(game.width/2, 325, "button", myrestart, this, 1, 0, 2, 0);
		this.restart_btn.scale.setTo(0.5, 0.5);
		this.restart_btn.anchor.setTo(0.5, 0.5);
		this.restart_btn.visible = false;
		this.restart_text = game.add.text(game.width/2, 325, "重開遊戲", this.text_style);
		this.restart_text.anchor.setTo(0.5, 0.5);
		this.restart_text.visible = false;
		this.restart_btn.onInputOver.add( function(){
			this.restart_text.setStyle(this.hover_style);
		}, this);
		this.restart_btn.onInputDown.add(function(){
			this.restart_text.setStyle(this.press_style);
		}, this);
		this.restart_btn.onInputUp.add(function(){
			this.restart_text.setStyle(this.hover_style);
		}, this);
		this.restart_btn.onInputOut.add(function(){
			this.restart_text.setStyle(this.text_style);
		}, this);

		this.back_to_menu_btn = game.add.button(game.width/2, 450, "button", back_to_menu, this, 1, 0, 2, 0);
		this.back_to_menu_btn.scale.setTo(0.5, 0.5);
		this.back_to_menu_btn.anchor.setTo(0.5, 0.5);
		this.back_to_menu_btn.visible = false;
		this.back_to_menu_text = game.add.text(game.width/2, 450, "回主選單", this.text_style);
		this.back_to_menu_text.anchor.setTo(0.5, 0.5);
		this.back_to_menu_text.visible = false;
		this.back_to_menu_btn.onInputOver.add( function(){
			this.back_to_menu_text.setStyle(this.hover_style);
		}, this);
		this.back_to_menu_btn.onInputDown.add(function(){
			this.back_to_menu_text.setStyle(this.press_style);
		}, this);
		this.back_to_menu_btn.onInputUp.add(function(){
			this.back_to_menu_text.setStyle(this.hover_style);
		}, this);
		this.back_to_menu_btn.onInputOut.add(function(){
			this.back_to_menu_text.setStyle(this.text_style);
		}, this);
	},
	update: function(){
		update_background(this);
		if(this.shoot_key.isDown)
			this.shooting = true;
		else if(this.shoot_key.isUp)
			this.shooting = false;
		if(this.count6 == 0){
			if(this.shooting){
				this.gunfire.visible = true;
				this.gunfire.animations.play("fire");
				this.firesound.play();
				let bullet = game.add.sprite(this.player.x+1.8*this.leg1.width, this.player.y+0.1*this.leg1.width, "player1_bullet");
				bullet.scale.setTo(0.5, 0.5);
				game.physics.arcade.enable(bullet);
				bullet.body.velocity.x = 1500;
				bullet.checkWorldBounds = true;
				bullet.outOfBoundsKill = true;
				this.bullets.add(bullet);
			} else
				this.gunfire.visible = false;
		}
		game.physics.arcade.overlap(this.enemies, this.bullets, hitenemy, null, this);
		game.physics.arcade.overlap(enemies_fire, this.player, hitplayer, null, this);
		move_player(this);
		this.enemies.forEach(function(e){
			if(e.y+e.body.velocity.y/60+e.height/4 >= game.height){
				e.y = game.height-e.height/4;
				e.body.velocity.y = 0;
			} else if(e.y+e.body.velocity.y/60-e.height/4 <= 0){
				e.y = e.height/4;
				e.body.velocity.y = 0;
			}
		});
		this.scoreboard.text = "Score: "+score;
	},
	create_enemy: function(){
		let n = game.rnd.between(2, 5);
		for(let i=0; i<n; i++){
			let e = game.add.sprite(game.width, game.rnd.between(0, game.height), "enemy1");
			e.health = 3;
			e.scale.setTo(-0.5, 0.5);
			e.anchor.setTo(1, 0.5);
			game.physics.arcade.enable(e);
			e.events.onOutOfBounds.add(function(){
				this.player.damage(1);
				if(this.player.health == 2)
					heart3.visible = false;
				else if(this.player.health == 1)
					heart2.visible = false;
				else
					game.state.start("restartmenu");
			}, this);
			e.body.velocity.setTo(game.rnd.between(-300, -100), game.rnd.between(200, 100)*game.rnd.pick([1, -1]));
			game.time.events.loop(game.rnd.between(500, 1000), change_dir, e);
			game.time.events.loop(1000, enemy_fire, e);
			e.checkWorldBounds = true;
			e.outOfBoundsKill = true;
			this.enemies.add(e);
		}
	}
}

function resume(){
	this.mask.visible = false;
	this.pauseboard.visible = false;

	this.resume_btn.visible = false;
	this.resume_text.visible = false;

	this.restart_btn.visible = false;
	this.restart_text.visible = false;

	this.back_to_menu_btn.visible = false;
	this.back_to_menu_text.visible = false;

	game.paused = false;
}

function myrestart(){
	game.paused = false;
	play_bgm.stop();
	game.state.start("play");
}

function enemy_fire(){
	if(!this.alive)
		return;
	let b = game.add.sprite(this.x, this.y, "enemy_fire");
	game.physics.arcade.enable(b);
	b.body.velocity.setTo(game.rnd.between(-500, -300), game.rnd.between(200, 100)*game.rnd.pick([1, -1]));
	b.checkWorldBounds = true;
	b.outOfBoundsKill = true;
	b.body.setCircle(20);
	enemies_fire.add(b);
}

function back_to_menu(){
	game.paused = false;
	play_bgm.stop();
	game.state.start("mainmenu");
}

function change_dir(){
	if(!this.alive)
		return;
	if(game.rnd.pick([0, 1]) == 0)
		return;
	this.body.velocity.setTo(game.rnd.between(-300, -100), game.rnd.between(200, 100)*game.rnd.pick([1, -1]));
}

function hitplayer(p, b){
	b.kill();
	p.damage(1);
	if(p.health == 2)
		heart3.visible = false;
	else if(p.health == 1)
		heart2.visible = false;
	else
		game.state.start("restartmenu");
}

function hitenemy(e, b){
	b.kill();
	hitsound[game.rnd.between(0, 5)].play();
	let hiteffect = game.add.sprite(e.x, e.y, "hiteffect");
	hiteffect.anchor.setTo(0.5, 0.5);
	hiteffect.scale.setTo(0.5, 0.5);
	hiteffect.animations.add("explosion", [0, 1, 2, 3], 8, false).play(16, false, true);
	e.damage(1);
	if(!e.alive)
		explosion.play();
	score += 100;
}

function blink(){
	if(game.rnd.pick([1, 2, 3]) == 1)
		game.add.tween(this.scale).to({y: 0}, 80).yoyo(true).start();
}

function create_background(state){
	state.background01_1 = game.add.image(0, 0, "play_background01", 0);
	state.background01_2 = game.add.image(game.width, 0, "play_background01", 0);
	state.background01_1.scale.setTo(1.25, 1.25);
	state.background01_2.scale.setTo(1.25, 1.25);
	state.background02_1 = game.add.image(0, game.height/5, "play_background01", 1);
	state.background02_2 = game.add.image(game.width, game.height/5, "play_background01", 1);
	state.background02_1.scale.setTo(1.25, 1);
	state.background02_2.scale.setTo(1.25, 1);
	state.background03 = game.add.image(0, game.height/2, "play_background02", 0);
	state.background03.scale.setTo(1.25, 0.625);
	state.background04 = game.add.image(game.width, game.height/2, "play_background02", 1);
	state.background04.scale.setTo(1.25, 0.625);
}

function update_background(state){
	if(state.background01_1.x < -game.width)
		state.background01_1.x = 2*game.width+state.background01_1.x-1;
	else
		state.background01_1.x -= 1;
	if(state.background01_2.x < -game.width)
		state.background01_2.x = 2*game.width+state.background01_2.x-1;
	else
		state.background01_2.x -= 1;

	if(state.background02_1.x < -game.width)
		state.background02_1.x = 2*game.width+state.background02_1.x-10;
	else
		state.background02_1.x -= 10;
	if(state.background02_2.x < -game.width)
		state.background02_2.x = 2*game.width+state.background02_2.x-10;
	else
		state.background02_2.x -= 10;

	if(state.background03.x < -game.width)
		state.background03.x = 2*game.width+state.background03.x-25;
	else
		state.background03.x -= 25;
	if(state.background04.x < -game.width)
		state.background04.x = 2*game.width+state.background04.x-25;
	else
		state.background04.x -= 25;
}

function create_player(state, num){
	let player = "player"+num;
	let player_x = 200;
	let player_y = game.height/2;
	let player_size = 0.5;

	// leg1
	state.leg1 = game.add.image(player_x, player_y, player+"_leg1");
	state.leg1.scale.setTo(player_size, player_size);
	state.leg1.anchor.setTo(0.8, 0);

	let w = state.leg1.width;

	state.gunfire = game.add.sprite(player_x+w*1.8, player_y+w*0.1, player+"_gunfire");
	state.gunfire.scale.setTo(player_size, player_size);
	state.gunfire.visible = false;

	// arm1
	state.arm1 = game.add.image(player_x+w*0.25, player_y-w*0.4, player+"_arm1");
	state.arm1.scale.setTo(player_size, player_size);

	// rocket
	state.rocket = game.add.image(player_x-w*0.8, player_y-w*0.5, player+"_rocket");
	state.rocket.scale.setTo(player_size, player_size);

	// body
	state.body = game.add.image(player_x, player_y-w*0.2, player+"_body");
	state.body.scale.setTo(player_size, player_size);
	state.body.anchor.setTo(0.5, 0.5);

	// weapon
	state.weapon = game.add.image(player_x, player_y, player+"_weapon");
	state.weapon.scale.setTo(player_size, player_size);

	// arm2
	state.arm2 = game.add.image(player_x-w*0.1, player_y-w*0.45, player+"_arm2");
	state.arm2.scale.setTo(player_size, player_size);

	// hair3
	state.hair3 = game.add.image(player_x+w*0.8, player_y-w*1.1, player+"_hair3");
	state.hair3.scale.setTo(player_size, player_size);
	state.hair3.anchor.setTo(0.5, 0);

	// head
	state.head = game.add.image(player_x-w*0.3, player_y-w*1.8, player+"_head");
	state.head.scale.setTo(player_size, player_size);

	// ear
	state.ear = game.add.image(player_x+w*0.4, player_y-w*0.8, player+"_ear");
	state.ear.scale.setTo(player_size, player_size);
	state.ear.anchor.setTo(0.5, 0.5);

	// eye1
	state.eye1 = game.add.image(player_x+w*0.63, player_y-w*0.68, player+"_eye1");
	state.eye1.scale.setTo(player_size, player_size);
	state.eye1.anchor.setTo(0, 0.5);
	game.time.events.loop(1000, blink, state.eye1);

	// hair1
	state.hair1 = game.add.image(player_x+w*0.4, player_y-w*1.8, player+"_hair1");
	state.hair1.scale.setTo(player_size, player_size);
	state.hair1.anchor.setTo(1, 0);

	// hair4
	state.hair4 = game.add.image(player_x+w*0.5, player_y-w*1, player+"_hair4");
	state.hair4.scale.setTo(player_size, player_size);
	state.hair4.anchor.setTo(0.5, 0);

	// hair2
	state.hair2 = game.add.image(player_x+w*0.4, player_y-w*1.8, player+"_hair2");
	state.hair2.scale.setTo(player_size, player_size);
	state.hair2.anchor.setTo(1, 0);

	// hair5
	state.hair5 = game.add.image(player_x+w*0.7, player_y-w*1.9, player+"_hair5");
	state.hair5.scale.setTo(player_size, player_size);
	state.hair5.anchor.setTo(0.5, 0);

	// leg2
	state.leg2 = game.add.image(player_x-w*1.25, player_y-w*0.12, player+"_leg2");
	state.leg2.scale.setTo(player_size, player_size);

	// player
	state.player = game.add.sprite(player_x, player_y, "hitarea");
	state.player.scale.setTo(0.4*player_size, 0.4*player_size);
	state.player.anchor.setTo(0.5, 0.5);
	state.player.health = 3;
	game.physics.arcade.enable(state.player);
	state.player.body.setCircle(45);

	// hitarea
	state.hitarea = game.add.image(player_x, player_y, "hitarea");
	state.hitarea.scale.setTo(0.3*player_size, 0.3*player_size);
	state.hitarea.anchor.setTo(0.5, 0.5);
}

function move_player(state){
	let dx, dy;
	dx = dy = 0;
	if(state.cursor.left.isDown){
		if(state.player.x-state.player.width/2-state.player_speed > 0)
			dx = -state.player_speed;
		else
			dx = -state.player.x+state.player.width/2;
	}
	if(state.cursor.right.isDown){
		if(state.player.x+state.player.width/2+state.player_speed < game.width)
			dx = state.player_speed;
		else
			dx = game.width-state.player.width/2-state.player.x;
	}
	if(state.cursor.up.isDown){
		if(state.player.y-state.player.height/2-state.player_speed > 0)
			dy = -state.player_speed;
		else
			dy = -state.player.y+state.player.height/2;

	}
	if(state.cursor.down.isDown){
		if(state.player.y+state.player.height/2+state.player_speed < game.height)
			dy = state.player_speed;
		else
			dy = game.height-state.player.height/2-state.player.y;
	}
	state.leg1.x += dx;
	state.leg1.y += dy;

	state.gunfire.x += dx;
	state.gunfire.y += dy;

	state.arm1.x += dx;
	state.arm1.y += dy;

	state.rocket.x += dx;
	state.rocket.y += dy;

	state.body.x += dx;
	state.body.y += dy;

	state.arm2.x += dx-state.d6;
	state.arm2.y += dy;

	state.weapon.x += dx-state.d6;
	state.weapon.y += dy;

	state.hair3.x += dx;
	state.hair3.y += dy;
	state.hair3.angle = state.count3/state.period_hair3*15;

	state.head.x += dx;
	state.head.y += dy;

	state.ear.x += dx;
	state.ear.y += dy;

	state.eye1.x += dx;
	state.eye1.y += dy;

	state.hair1.x += dx;
	state.hair1.y += dy;
	state.hair1.angle = state.count1/state.period_hair1*(-15);

	state.hair4.x += dx;
	state.hair4.y += dy;
	state.hair4.angle = state.count4/state.period_hair4*(-15);

	state.hair2.x += dx;
	state.hair2.y += dy;
	state.hair2.angle = state.count2/state.period_hair2*(-15);

	state.hair5.x += dx;
	state.hair5.y += dy;
	state.hair5.angle = state.count5/state.period_hair5*(-5);

	state.leg2.x += dx;
	state.leg2.y += dy;

	state.hitarea.x += dx;
	state.hitarea.y += dy;

	state.player.x += dx;
	state.player.y += dy;

	update_count(state);
}

function update_count(state){
	if(state.count1 == state.period_hair1)
		state.d1 = -1;
	else if(state.count1 == 0)
		state.d1 = 1;
	state.count1 += state.d1;

	if(state.count2 == state.period_hair2)
		state.d2 = -1;
	else if(state.count2 == 0)
		state.d2 = 1;
	state.count2 += state.d2;

	if(state.count3 == state.period_hair3)
		state.d3 = -1;
	else if(state.count3 == 0)
		state.d3 = 1;
	state.count3 += state.d3;

	if(state.count4 == state.period_hair4)
		state.d4 = -1;
	else if(state.count4 == 0)
		state.d4 = 1;
	state.count4 += state.d4;

	if(state.count5 == state.period_hair5)
		state.d5 = -1;
	else if(state.count5 == 0)
		state.d5 = 1;
	state.count5 += state.d5;

	if(state.count6 == state.period_shoot)
		state.d6 = -1;
	else if(state.count6 == 0){
		if(state.shooting)
			state.d6 = 1;
		else
			state.d6 = 0;
	}
	state.count6 += state.d6;

}
