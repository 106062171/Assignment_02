let boot_state = {
	preload: function(){
		game.load.image("mainmenu_background", "assets/mainmenu_background.png");
		game.load.image("restartmenu_background", "assets/restartmenu_background.png");
		game.load.image("hitarea", "assets/hitarea.png");
		game.load.image("enemy1", "assets/enemy1.png");
		game.load.image("enemy_fire", "assets/enemy_fire.png");
		game.load.image("scoreboard", "assets/scoreboard.png");
		game.load.image("heart", "assets/heart.png");
		game.load.image("board", "assets/board.png");
		game.load.image("mask", "assets/mask.png");
		game.load.image("cursorkey", "assets/cursorkey.png");
		game.load.image("keyboardZ", "assets/keyboardZ.png");
		game.load.audio("menu_bgm", "assets/menu_bgm.mp3");
		game.load.audio("play_bgm", "assets/play_bgm.mp3");
		game.load.audio("hiteffect1", "assets/hitenemy1.mp3");
		game.load.audio("hiteffect2", "assets/hitenemy2.mp3");
		game.load.audio("hiteffect3", "assets/hitenemy3.mp3");
		game.load.audio("hiteffect4", "assets/hitenemy4.mp3");
		game.load.audio("hiteffect5", "assets/hitenemy5.mp3");
		game.load.audio("hiteffect6", "assets/hitenemy6.mp3");
		game.load.audio("firesound", "assets/firesound.mp3");
		game.load.audio("explosion", "assets/explosion.mp3");
		for(let i=1; i<2; i++){
			game.load.image("player"+i+"_head", "assets/player"+i+"_head.png");
			game.load.image("player"+i+"_bullet", "assets/player"+i+"_bullet.png");
			game.load.spritesheet("player"+i+"_gunfire", "assets/player"+i+"_gunfire.png", 50, 50, 4);
			game.load.image("player"+i+"_eye1", "assets/player"+i+"_eye1.png");
			game.load.image("player"+i+"_ear", "assets/player"+i+"_ear.png");
			game.load.image("player"+i+"_weapon", "assets/player"+i+"_weapon.png");
			game.load.image("player"+i+"_rocket", "assets/player"+i+"_rocket.png");
			game.load.image("player"+i+"_leg1", "assets/player"+i+"_leg1.png");
			game.load.image("player"+i+"_leg2", "assets/player"+i+"_leg2.png");
			game.load.image("player"+i+"_arm1", "assets/player"+i+"_arm1.png");
			game.load.image("player"+i+"_arm2", "assets/player"+i+"_arm2.png");
			game.load.image("player"+i+"_hair1", "assets/player"+i+"_hair1.png");
			game.load.image("player"+i+"_hair2", "assets/player"+i+"_hair2.png");
			game.load.image("player"+i+"_hair3", "assets/player"+i+"_hair3.png");
			game.load.image("player"+i+"_hair4", "assets/player"+i+"_hair4.png");
			game.load.image("player"+i+"_hair5", "assets/player"+i+"_hair5.png");
			game.load.image("player"+i+"_body", "assets/player"+i+"_body.png");
		}
		game.load.spritesheet("button", "assets/button.png", 579, 229, 3);
		game.load.spritesheet("play_background01", "assets/play_background01.png", 1024, 512, 2);
		game.load.spritesheet("play_background02", "assets/play_background02.png", 1024, 512, 2);
		game.load.spritesheet("hiteffect", "assets/hiteffect.png", 256, 256, 4);
	},
	create: function(){
		game.stage.backgroundColor = "#000000";
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.renderer.renderSession.roundPixels = true;
		game.state.start("mainmenu");
	}
}
