let game = new Phaser.Game(1280, 640, Phaser.AUTO, "canvas");

game.state.add('boot', boot_state);
game.state.add('mainmenu', mainmenu_state);
game.state.add('play', play_state);
game.state.add('restartmenu', restartmenu_state);

game.state.start('boot');
