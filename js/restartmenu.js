let restartmenu_state = {
	create: function(){
		this.text_style = {font: "36px Arial", fill: "White"};
		this.hover_style = {font: "36px Arial", fill: "LightBlue"};
		this.press_style = {font: "36px Arial", fill: "FireBrick"};

		game.add.image(0, 0, "restartmenu_background");

		this.board = game.add.image(game.width/4*3-50, game.height/2, "board");
		this.gameover = game.add.text(game.width/4*3-50, game.height/2-150, "GAME OVER T_T", {font: "64px Arial", fill: "White"});
		this.gameover.anchor.setTo(0.5, 0.5);
		this.scoreboard = game.add.text(game.width/4*3-200, game.height/2, "Score: "+score, {font: "52px Arial", fill: "White"});
		this.scoreboard.anchor.setTo(0.5, 0.5);
		this.board.anchor.setTo(0.5, 0.5);
		this.board.scale.setTo(1.3, 1);
		this.restart_btn = game.add.button(game.width/4, 250, "button", restart, this, 1, 0, 2, 0);
		this.restart_btn.scale.setTo(0.5, 0.5);
		this.restart_btn.anchor.setTo(0.5, 0.5);
		this.restart_text = game.add.text(game.width/4, 250, "重開遊戲", this.text_style);
		this.restart_text.anchor.setTo(0.5, 0.5);
		this.restart_btn.onInputOver.add( function(){
			this.restart_text.setStyle(this.hover_style);
		}, this);
		this.restart_btn.onInputDown.add(function(){
			this.restart_text.setStyle(this.press_style);
		}, this);
		this.restart_btn.onInputUp.add(function(){
			this.restart_text.setStyle(this.hover_style);
		}, this);
		this.restart_btn.onInputOut.add(function(){
			this.restart_text.setStyle(this.text_style);
		}, this);

		this.back_to_menu_btn = game.add.button(game.width/4, 400, "button", back_to_menu, this, 1, 0, 2, 0);
		this.back_to_menu_btn.scale.setTo(0.5, 0.5);
		this.back_to_menu_btn.anchor.setTo(0.5, 0.5);
		this.back_to_menu_text = game.add.text(game.width/4, 400, "回主選單", this.text_style);
		this.back_to_menu_text.anchor.setTo(0.5, 0.5);
		this.back_to_menu_btn.onInputOver.add( function(){
			this.back_to_menu_text.setStyle(this.hover_style);
		}, this);
		this.back_to_menu_btn.onInputDown.add(function(){
			this.back_to_menu_text.setStyle(this.press_style);
		}, this);
		this.back_to_menu_btn.onInputUp.add(function(){
			this.back_to_menu_text.setStyle(this.hover_style);
		}, this);
		this.back_to_menu_btn.onInputOut.add(function(){
			this.back_to_menu_text.setStyle(this.text_style);
		}, this);
	}
}

function restart(){
	game.paused = false;
	play_bgm.stop();
	play_bgm.play();
	game.state.start("play");
}

function back_to_menu(){
	game.paused = false;
	play_bgm.stop();
	game.state.start("mainmenu");
}
