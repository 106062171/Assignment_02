let mainmenu_state = {
	create: function(){
		this.text_style = {font: "36px Arial", fill: "White"};
		this.hover_style = {font: "36px Arial", fill: "LightBlue"};
		this.press_style = {font: "36px Arial", fill: "FireBrick"};

		game.add.image(0, 0, "mainmenu_background");
		this.keyboardZ = game.input.keyboard.addKey(Phaser.KeyCode.Z);
		this.keyboardZ.onDown.add(function(state){
			this.keyboardZ.visible = false;
			this.cursorkey.visible = false;
			this.instruction_move.visible = false;
			this.instruction_shoot.visible = false;
			this.start_btn.visible = true;
			this.start_text.visible = true;
			this.guide_btn.visible = true;
			this.guide_text.visible = true;
			this.mask.visible = false;
			this.board.visible = false;
			this.instruction_Z.visible = false;
		}, this);
		this.mask = game.add.image(0, 0, "mask");
		this.mask.visible = false;
		this.board = game.add.image(game.width/2, game.height/2, "board");
		this.board.anchor.setTo(0.5, 0.5);
		this.board.visible = false;
		this.instruction_move = game.add.text(game.width/2-180, game.height/2-100, "移動:", this.text_style);
		this.cursorkey = game.add.image(game.width/2+80, game.height/2-100, "cursorkey");
		this.instruction_shoot = game.add.text(game.width/2-180, game.height/2+80, "射擊:", this.text_style);
		this.keyboardZ = game.add.image(game.width/2+80, game.height/2+90, "keyboardZ");
		this.instruction_Z = game.add.text(game.width/2, game.height-50, "Press [z] to go back to menu", this.text_style);
		this.instruction_Z.anchor.setTo(0.5, 0.5);
		this.keyboardZ.anchor.setTo(0.5, 0.5);
		this.keyboardZ.scale.setTo(0.35, 0.35);
		this.cursorkey.anchor.setTo(0.5, 0.5);
		this.cursorkey.scale.setTo(0.4, 0.4);
		this.keyboardZ.visible = false;
		this.cursorkey.visible = false;
		this.instruction_move.visible = false;
		this.instruction_shoot.visible = false;
		this.instruction_Z.visible = false;

		this.bgm = game.add.audio("menu_bgm", 1, true);
		this.bgm.play();

		// create start button
		this.start_btn = game.add.button(game.width/4, 250, "button", game_start, this, 1, 0, 2, 0);
		this.start_btn.scale.setTo(0.5, 0.5);
		this.start_btn.anchor.setTo(0.5, 0.5);
		this.start_text = game.add.text(game.width/4, 250, "開始遊戲", this.text_style);
		this.start_text.anchor.setTo(0.5, 0.5);
		this.start_btn.onInputOver.add( function(){
			this.start_text.setStyle(this.hover_style);
		}, this);
		this.start_btn.onInputDown.add( function(){
			this.start_text.setStyle(this.press_style);
		}, this);
		this.start_btn.onInputUp.add( function(){
			this.start_text.setStyle(this.hover_style);
		}, this);
		this.start_btn.onInputOut.add( function(){
			this.start_text.setStyle(this.text_style);
		}, this);

		// create guide button
		this.guide_btn = game.add.button(game.width/4, 400, "button", guide, this, 1, 0, 2, 0);
		this.guide_btn.scale.setTo(0.5, 0.5);
		this.guide_btn.anchor.setTo(0.5, 0.5);
		this.guide_text = game.add.text(game.width/4, 400, "操作說明", this.text_style);
		this.guide_text.anchor.setTo(0.5, 0.5);
		this.guide_btn.onInputOver.add( function(){
			this.guide_text.setStyle(this.hover_style);
		}, this);
		this.guide_btn.onInputDown.add( function(){
			this.guide_text.setStyle(this.press_style);
		}, this);
		this.guide_btn.onInputUp.add( function(){
			this.guide_text.setStyle(this.hover_style);
		}, this);
		this.guide_btn.onInputOut.add( function(){
			this.guide_text.setStyle(this.text_style);
		}, this);

	}
}

function game_start(){
	this.bgm.stop();
	game.state.start("play");
}

function guide(){
	this.keyboardZ.visible = true;
	this.cursorkey.visible = true;
	this.instruction_move.visible = true;
	this.instruction_shoot.visible = true;
	this.start_btn.visible = false;
	this.start_text.visible = false;
	this.guide_btn.visible = false;
	this.guide_text.visible = false;
	this.mask.visible = true;
	this.board.visible = true;
	this.instruction_Z.visible = true;
}
